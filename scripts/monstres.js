//Liste des monstres
var Lmonstres = [

	//Kitsune
	{ id: 0, name: "Shihwa", img: "Images/Kitsune/Shihwa.jpg", poss: 0},
	{ id: 1, name: "Soha", img: "Images/Kitsune/Soha.jpg", poss: 0},
	{ id: 2, name: "Arang", img: "Images/Kitsune/Arang.jpg", poss: 0},
	{ id: 3, name: "Chamie", img: "Images/Kitsune/Chamie.jpg", poss: 0},
	{ id: 4, name: "Kamiya", img: "Images/Kitsune/Kamiya.png", poss: 0},

	//Ondine
	{ id: 5, name: "Atenai", img: "Images/Ondine/Atenai.jpg", poss: 0},
	{ id: 6, name: "Mikene", img: "Images/Ondine/Mikene.png", poss: 0},
	{ id: 7, name: "Delphoi", img: "Images/Ondine/Delphoi.jpg", poss: 0},
	{ id: 8, name: "Icasha", img: "Images/Ondine/Icasha.jpg", poss: 0},
	{ id: 9, name: "Tilasha", img: "Images/Ondine/Tilasha.jpg", poss: 0},

	//Sylphe
	{ id: 10, name: "Baretta", img: "Images/Sylphe/Baretta.png", poss: 0},
	{ id: 11, name: "Tyron", img: "Images/Sylphe/Tyron.png", poss: 0},
	{ id: 12, name: "Shimitae", img: "Images/Sylphe/Shimitae.png", poss: 0},
	{ id: 13, name: "Eredas", img: "Images/Sylphe/Eredas.png", poss: 0},
	{ id: 14, name: "Aschubel", img: "Images/Sylphe/Aschubel.png", poss: 0},

	//Sylphide
	{ id: 15, name: "Fria", img: "Images/Sylphide/Fria.png", poss: 0},
	{ id: 16, name: "Lumirecia", img: "Images/Sylphide/Lumirecia.jpg", poss: 0},
	{ id: 17, name: "Acasis", img: "Images/Sylphide/Acasis.png", poss: 0},
	{ id: 18, name: "Mihael", img: "Images/Sylphide/Mihael.png", poss: 0},
	{ id: 19, name: "Icares", img: "Images/Sylphide/Icares.jpg", poss: 0},

	//Anubis 
	{ id: 20, name: "Khmun", img: "Images/Anubis/Khmun.png", poss: 0},
	{ id: 21, name: "Avaris", img: " ", poss: 0},
	{ id: 22, name: "Iunu", img: " ", poss: 0},
	{ id: 23, name: "Amarna", img: " ", poss: 0},
	{ id: 24, name: "Thebae", img: " ", poss: 0},

	//Loup Garou
	{ id: 25, name: "Garoche", img: " ", poss: 0},
	{ id: 26, name: "Vigor", img: "Images/LoupGarou/Vigor.png", poss: 0},
	{ id: 27, name: "Shakan", img: " ", poss: 0},
	{ id: 28, name: "Eshir", img: " ", poss: 0},
	{ id: 29, name: "Jultan", img: " ", poss: 0},

	//Dame de l'enfer
	{ id: 30, name: "Raki", img: " ", poss: 0},
	{ id: 31, name: "Beth", img: " ", poss: 0},
	{ id: 32, name: "Ethna", img: "Images/DameDeLenfer/Ethna.jpg", poss: 0},
	{ id: 33, name: "Asima", img: " ", poss: 0},
	{ id: 34, name: "Cracka", img: " ", poss: 0},

	//Reine du desert
	{ id: 35, name: "Sekhmet", img: " ", poss: 0},
	{ id: 36, name: "Bastet", img: "Images/ReineDuDesert/Bastet.png", poss: 0},
	{ id: 37, name: "Hathor", img: " ", poss: 0},
	{ id: 38, name: "Isis", img: " ", poss: 0},
	{ id: 39, name: "Nephtys", img: " ", poss: 0},

	//Empereur de la foudre
	{ id: 40, name: "Baleygr", img: " ", poss: 0},
	{ id: 41, name: "Bolverk", img: "Images/EmpDeFoudre/Bolverk.jpg", poss: 0},
	{ id: 42, name: "Odin", img: "Images/EmpDeFoudre/Odin.jpg", poss: 0},
	{ id: 43, name: "Geldnir", img: " ", poss: 0},
	{ id: 44, name: "Herteit", img: "Images/EmpDeFoudre/Herteit.jpg"},

	//Phenix
	{ id: 45, name: "Perna", img: "Images/Phenix/Perna.jpg", poss: 0},
	{ id: 46, name: "Sigmarus", img: " ", poss: 0},
	{ id: 47, name: "Teshar", img: "Images/Phenix/Teshar.jpg", poss: 0},
	{ id: 48, name: "Eludia", img: "Images/Phenix/Eludia.jpg", poss: 0},
	{ id: 49, name: "Jaara", img: "Images/Phenix/Jaara.jpg", poss: 0},

	//Chakrams
	{ id: 50, name: "Shaina", img: "Images/Chakrams/Shaina.jpg", poss: 0},
	{ id: 51, name: "Talia", img: "Images/Chakrams/Talia.jpg", poss: 0},
	{ id: 52, name: "Melissa", img: " ", poss: 0},
	{ id: 53, name: "Deva", img: "Images/Chakrams/Deva.jpg", poss: 0},
	{ id: 54, name: "Belita", img: " ", poss: 0},

	//Boomerang
	{ id: 55, name: "Maruna", img: "Images/Boomerang/Maruna.jpg", poss: 0},
	{ id: 56, name: "Sabrina", img: "Images/Boomerang/Sabrina.jpg", poss: 0},
	{ id: 57, name: "Zenobia", img: "Images/Boomerang/Zenobia.jpg", poss: 0},
	{ id: 58, name: "Bailey", img: "Images/Boomerang/Bailey.jpg", poss: 0},
	{ id: 59, name: "Martina", img: "Images/Boomerang/Martina.jpg", poss: 0},

	//Harpe
	{ id: 60, name: "Harmonia", img: "Images/Harpe/Harmonia.jpg", poss: 0},
	{ id: 61, name: "Sonnet", img: "Images/Harpe/Sonnet.jpg", poss: 0},
	{ id: 62, name: "Triana", img: "Images/Harpe/Triana.png", poss: 0},
	{ id: 63, name: "Celia", img: "Images/Harpe/Celia.jpg", poss: 0},
	{ id: 64, name: "Vivachel", img: "Images/Harpe/Vivachel", poss: 0}




];